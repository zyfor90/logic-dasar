# Logic Dasar

## SOAL 1

Soal:
> buatlah angka 212312 menjadi seperti berikut

Hasil yang diinginkan:
> 200000\
10000\
2000\
300\
10\
2

Jawaban:
```php
<?php
function testing ($angka) {
	for ($i=0; $i < strlen($angka); $i++) {
		echo round((int) substr($angka, $i), -abs(strlen($angka) - $i - 1)) . "\n";
	}
}

return testing(angka: 212312);
```

## SOAL 2

Soal:
> buatlah 1 function seperti dibawah. carilah index sebelumnya + index selanjutnya = target, jika benar maka keluarkan index nya.\
$nums = [2,4,7,10];\
$target = 9;\
function($nums, $target);

Hasil yang diinginkan:
>$index = [0, 2] (karena index 0 + index 2 = target) / (2 + 7 = 9)

Jawaban:
```php
<?php
function testing ($nums, $target) {
	$array = [];
	for ($i=0; $i < count($nums); $i++) {
		for ($a=0; $a < count($nums); $a++) {
			if ($nums[$i]+$nums[$a] == $target) {
				$array[] = $i;
			}
		}
	}
	
	print_r($array);
}

return testing(target: 9, nums: [2,8,7,9]);
```

## SOAL 3
Soal:
> buatlah function seperti dibawah, kemudian buatlah kelipatan 3 menjadi Bam, dan kelipatan 5 menjadi Buu, jika angka kelipatan 3 dan 5 sama maka buatlah menjadi BamBuu

Hasil yang diinginkan:
>1
2
Bam
4
Buu
Bam
7
8
Bam
Buu
11
Bam
13
14
BamBuu

Jawaban:
```php
function BamBuu($number) {
    for($i=1; $i <= $number; $i++) {
        if(is_int($i / 3) == 1 && is_int($i / 5) == 1) {
            $result = 'BamBuu';
        } else if (is_int($i / 3) == 1) {
            $result = 'Bam';
        } else if (is_int($i / 5) == 1) {
            $result = 'Buu';
        } else {
            $result = $i;
        }
        
        echo $result . "\n";
    }

}

return BamBuu(number: 15);
``

